//
//  YTViewController.m
//  YTBaseDBManager
//
//  Created by flypigrmvb on 12/07/2017.
//  Copyright (c) 2017 flypigrmvb. All rights reserved.
//

#import "YTViewController.h"
#import "VideoUploadModel.h"

@interface YTViewController ()

@end

@implementation YTViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    NSLog(@"Home = %@", NSHomeDirectory());
    
    [self insertData];
}

- (void)insertData {
    VideoUploadModel *data = [VideoUploadModel new];
    data.itemID = rand();
    data.userId = [NSString stringWithFormat:@"%@-%@", @(rand()), @(rand())];
    [VideoUploadModel insertData:data error:nil];
}

@end
