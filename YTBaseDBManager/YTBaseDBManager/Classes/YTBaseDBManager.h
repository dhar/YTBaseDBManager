//
//  YTBaseDBManager.h
//  Pods
//
//  Created by aron on 2017/11/14.
//
//

#import <Foundation/Foundation.h>
#import <FMDB/FMDB.h>

@interface YTBaseDBManager : NSObject

@property (nonatomic, strong, readonly) FMDatabaseQueue *databaseQueue;///<数据库操作Queue

+ (instancetype)sharedInstance;

/// 升级数据库
/// @param DBFilePath 数据库文件
/// @param newDBVersion 新版本
/// @param createTableBlock 创建表逻辑
- (void)startUpgradeDBWithPath:(NSString *)DBFilePath newDBVersion:(NSString*)newDBVersion createTableBlock:(void(^)(void))createTableBlock;

@end
